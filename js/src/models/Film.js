define([
    'backbone',
    'src/utils/config'
], function (
    Backbone,
    config
) {
    'use strict';

    return Backbone.Model.extend({

        // These will be stored in the proto
        defaults: {
            'watchlist': false
        },


        //http://api.themoviedb.org/3/movie/62177?api_key=6b5ff1a81fe75e221731a3f81daedcfe
        url: function() {
            var url = 'http://api.themoviedb.org/3/movie/'+this.id+'?api_key='+config.apiKey+'';

            return url;
        },


        /**
         *  Once the collection is set up and returns the data you need
         *  the models parse method can be used to amend the data to look
         *  exactly how you want it to be before passing it off to the view to be
         *  templated out.
         *
         *  NB in this instance I've created a completely new object which I return
         *  and this becomes the official data for the collection, if I only had to
         *  update a few values I'd probably just update response. and then return response
         */
        parse: function(response) {

            // This stops circular dependencies
            var dataStore = require('src/utils/dataStore');


            if(response.poster_path) {
                response.poster = 'http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w500'+response.poster_path;
            } else {
                response.poster = 'img/coming-soon-small.jpg';
            }

            if (response.release_date) {
                var year = response.release_date.substring(0, 4);
                var month = (response.release_date.substring(5, 7)) - 1;
                var day = response.release_date.substring( 8 );
                response.released = new Date(year, month, day);
                response.releaseYear = response.released.getFullYear();
            }

            if(response.backdrop_path) {
                response.backdrop = 'http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w780'+response.backdrop_path;
            }

            if( dataStore.watchListFilms.get(response.id) ) {
                response.watchlist = true;
            } else {
                response.watchlist = false;
            }

            return response;
        }

    });
});