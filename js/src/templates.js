define(['handlebars'], function(Handlebars) {

this["templates"] = this["templates"] || {};

this["templates"]["film"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        <img src=\"";
  if (stack1 = helpers.backdrop) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.backdrop; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"film-backdrop\">\n    ";
  return buffer;
  }

function program3(depth0,data) {
  
  
  return "\n        <img src=\"img/coming-soon.jpg\" alt=\"\" class=\"film-backdrop\">\n    ";
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n            <blockquote>";
  if (stack1 = helpers.tagline) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tagline; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</blockquote>\n        ";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n                    <li>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</li>\n                ";
  return buffer;
  }

function program9(depth0,data) {
  
  
  return "\n            <button class=\"btn btn-watchlist btn-watchlist-remove js-watchlist\" data-toggle=\"remove\">-</button>\n        ";
  }

function program11(depth0,data) {
  
  
  return "\n            <button class=\"btn btn-watchlist btn-watchlist-add js-watchlist\" data-toggle=\"add\">+</button>\n        ";
  }

  buffer += "<div class=\"film clearfix\" data-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n\n    ";
  stack1 = helpers['if'].call(depth0, depth0.backdrop, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n    <div class=\"film-details\">\n\n        ";
  stack1 = helpers['if'].call(depth0, depth0.tagline, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n        <h2 class=\"film-title\">";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h2>\n        <p class=\"film-release-date\">Released ";
  if (stack1 = helpers.releaseYear) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.releaseYear; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n\n        <p class=\"film-overview\">";
  if (stack1 = helpers.overview) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.overview; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n\n        <ul class=\"stats-tabs\">\n            <li>";
  if (stack1 = helpers.vote_average) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vote_average; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " <span>Vote Average</span></li>\n            <li>";
  if (stack1 = helpers.vote_count) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vote_count; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " <span>Vote Count</span></li>\n        </ul>\n\n        <p>Adult: ";
  if (stack1 = helpers.adult) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.adult; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n        <p>Budget: ";
  if (stack1 = helpers.budget) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.budget; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n\n        <div>\n            <h3>Genres</h3>\n\n            <ul>\n                ";
  stack1 = helpers.each.call(depth0, depth0.genres, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n            </ul>\n        </div>\n    </div>\n\n    <div class=\"cta-ctn\">\n        ";
  stack1 = helpers['if'].call(depth0, depth0.watchlist, {hash:{},inverse:self.program(11, program11, data),fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    </div>\n\n    <a href=\"#\" class=\"btn btn-back\">Back to results</a>\n</div>";
  return buffer;
  });

this["templates"]["films"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "\n        <button class=\"btn btn-watchlist btn-watchlist-remove js-watchlist\" data-toggle=\"remove\">-</button>\n    ";
  }

function program3(depth0,data) {
  
  
  return "\n        <button class=\"btn btn-watchlist btn-watchlist-add js-watchlist\" data-toggle=\"add\">+</button>\n    ";
  }

  buffer += "<a href=\"/#film/";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"film-entry js-film-entry\" data-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n    <img src=\"";
  if (stack1 = helpers.poster) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.poster; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"film-img\">\n\n    <div class=\"result-film-details\">\n        <h2 class=\"film-title\">";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h2>\n        <p class=\"film-release-date\">Released ";
  if (stack1 = helpers.releaseYear) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.releaseYear; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n\n        <ul class=\"result-stats-tabs clearfix\">\n            <li>";
  if (stack1 = helpers.vote_average) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vote_average; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " <span>Vote Average</span></li>\n            <li>";
  if (stack1 = helpers.vote_count) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vote_count; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " <span>Vote Count</span></li>\n        </ul>\n    </div>\n</a>\n<div class=\"cta-ctn\">\n    ";
  stack1 = helpers['if'].call(depth0, depth0.watchlist, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>\n";
  return buffer;
  });

this["templates"]["mainApp"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<header class=\"search-container\">\n    <form action=\"/\" class=\"search\">\n        <fieldset>\n            <input type=\"text\" placeholder=\"Search for a movie...\" class=\"search-box\" />\n            <input type=\"submit\" value=\"Search\" class=\"btn btn-search js-btn-search\" />\n        </fieldset>\n    </form>\n</header>\n\n<a href=\"/watchlist\" class=\"view-watchlist js-view-watchlist\">View Watchlist</a>";
  });

this["templates"]["watchlist"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"watchlist-view\">\n    <h2>My Watchlist</h2>\n\n    <div class=\"watchlist-results\"></div>\n</div>";
  });

return this["templates"];

});