define([
    'backbone',
    'src/utils/config',
    'src/views/SearchResultsView',
    'src/views/DetailedFilmView',
    'src/views/WatchlistView',
    'src/utils/dataStore',
    'src/router',
    'src/models/Film',
    'templates',
    'jquery'
], function(
    Backbone,
    config,
    SearchResultsView,
    DetailedFilmView,
    WatchlistView,
    dataStore,
    router,
    Film,
    templates,
    $
) {
    'use strict';

    return Backbone.View.extend({

        template: templates.mainApp,


        events: {
            'click .js-btn-search': 'onSearch',
            'click .js-view-watchlist': 'onViewWatchlistClick'
        },


        initialize: function() {
            this.selectors = {};

            Backbone.Events.on('show:home', this.onShowHome, this);
            Backbone.Events.on('search:film', this.onShowFilm, this);

            Backbone.Events.on('add:watchlist', this.onWatchlistAdd, this);
            Backbone.Events.on('remove:watchlist', this.onWatchlistRemove, this);
            Backbone.Events.on('show:watchlist', this.onShowWatchlist, this);
        },


        render: function() {

            // So inject the base mainApp template into the DOM, specifically inside #main which is $el
            this.$el.html(this.template());

            this.selectors.searchBox = this.$el.find('.search-box');

            return this;

            /**
             *  /\/\/\/\/\/\/\/\/\
             *  Right now the search box has been injected to the DOM and that's it
             *  /\/\/\/\/\/\/\/\/\
             */
        },



        /**
         *  So onShowHome can be called by either running a search
         *  or by deeplinking into the page so '' or 'search/:term'
         *  This means our function needs to be able to accept the search term
         *
         *  called on search
         */
        onShowHome: function(searchTerm) {

            // First we check to see if we are in the 'Detailed View'
            // If so we want to remove this and set it to null
            if( this.detailedFilm ) {
                this.detailedFilm.remove();
                this.detailedFilm = null;
            }

            if( this.watchListView ) {
                this.watchListView.remove();
                this.watchListView = null;
            }


            // We then want to create our searchResults View using the collection set in dataStore for Films
            this.searchResults = new SearchResultsView({
                'collection': dataStore.films
            });


            // Set search term in input
            // We check to see if a search term exists
            // If it does we have to revert the URI encoding used in the search pulp%20fiction
            // Set the search box to contain this search term as that makes sense
            // Then perform a search using the search term
            if( searchTerm ) {
                var searchTerm = decodeURIComponent(searchTerm);
                this.selectors.searchBox.val(searchTerm);
                this.performSearch(searchTerm);
            }

            // When we have the correct data we then have to render our results and append to our results container
            // this.$el = #main which is passed through as a param when instantiating the MainAppView in app.js
            this.$el.append(this.searchResults.render().$el);
        },


        /**
         *  Simple function to help
         */
        performSearch: function(searchTerm) {

            // So trim any whitespace to make sure the word being used in the search is totally correct
            var search = $.trim(searchTerm);

            // Quick check if the search is empty then do nothing
            if(search.length <= 0) {
                return false;
            }

            // Make the fetch using our search term
            dataStore.films.getFilms(searchTerm);

        },


        /**
         *  Gets run on search form submit
         */
        onSearch: function(e) {
            e.preventDefault();


            // So we get the search term
            var searchTerm = this.selectors.searchBox.val();


            // Pass the search term into the performSearch function
            // which basically fetches the data based on the term
            this.performSearch(searchTerm);

            // Now we need to update the url to reflect the search
            router.navigate('search/'+encodeURIComponent(searchTerm), { trigger:false });


            if( this.watchListView ) {
                this.watchListView.remove();
                this.watchListView = null;
                this.onShowHome();
            }


            // If we do a search when already on a 'Detailed View'
            // We reset the collection and then run our function to take us back to homepage/search
            if( this.detailedFilm ) {
                dataStore.films.reset();
                this.onShowHome();
            }


        },


        // Will recieve object or string relating to the film either from clicking a film OR from deeplinking
        // not used in main search results but fired once clickig into a SearchResultView
        onShowFilm: function(film) {

            // First thing we have to do is run a check to see make sure what
            // we are using is a model/object and if it isn't then convert it
            // to be one. This is important as this method will used events
            // being fired off by various actions like the search or deeplinking
            if( typeof(film) === 'string' ) {
                film = new Film({
                    id: film
                });
            }

            // hide results view, remove any handlers etc
            if( this.searchResults ) {
                this.searchResults.remove();
                this.searchResults = null;
            }


            // hide watchlist view
            if( this.watchListView ) {
                this.watchListView.remove();
                this.watchListView = null;
            }

            // create detailed film view
            this.detailedFilm = new DetailedFilmView({
                'model': film
            });

            this.$el.append(this.detailedFilm.$el);

        },


        /**
         *  When a user clicks the add to watchlist button the model is collected
         *  which is stored on the elements data-id attribute, the model is
         *  found in the collection and then passed to onWatchlistAdd which is
         *  listening in the initialize method for add:watchlist. What we need to
         *  here is first of all clone the models record before adding it to our
         *  newly created watchlist collection
         *
         *  @param is the watchlist model
         */
        onWatchlistAdd: function(film) {

            film.set({
                'watchlist': true
            });

            var clonedFilm = film.clone();

            dataStore.watchListFilms.add(clonedFilm);

        },


        onWatchlistRemove: function(film) {

            film.set({
                'watchlist': false
            });

            dataStore.watchListFilms.remove(film);
        },


        onViewWatchlistClick: function(e) {
            e.preventDefault();


            // Set the search box to be empty
            this.selectors.searchBox.val('');

            // Now we need to update the url to reflect the search
            router.navigate('watchlist/', { trigger:false });


            this.onShowWatchlist();
        },


        onShowWatchlist: function() {

            this.watchListView = new WatchlistView({
                'collection': dataStore.watchListFilms
            });

            // Hide search results
            if( this.searchResults ) {
                this.searchResults.remove();
                this.searchResults = null;
            }

            // Hide detailed film view if it is present
            if( this.detailedFilm ) {
                this.detailedFilm.remove();
                this.detailedFilm = null;
            }

            this.$el.append(this.watchListView.$el);

            this.watchListView.render();

        }

    });
});