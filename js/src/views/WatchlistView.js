define([
    'backbone',
    'templates',
    'src/views/ResultView',
    'src/utils/utils'
], function(
    Backbone,
    templates,
    ResultView,
    Utils
) {
    'use strict';

    return Backbone.View.extend({


        className: 'watchlist-view main-ctn',


        template: templates.watchlist,


        events: {
            'click .js-film-entry': 'onFilmEntryClick',
            'click .js-watchlist': 'onRemoveWatchlistFilmClick'
        },


        initialize: function() {

        },


        render: function() {
            var self = this,
                gridFragment = this.createItems();

                this.$el.html(gridFragment);


            return this;
        },


        createItems: function() {

            var self = this,
                gridFragment = document.createDocumentFragment();

                this.collection.each(function (film) {
                    var filmView = new ResultView({
                        'model': film
                    });

                    gridFragment.appendChild(filmView.el);

                }, this);


            return gridFragment;

        },


        onFilmEntryClick: function(e) {
            e.preventDefault();

            // Get the clicked elements film id
            var filmId = $(e.currentTarget).attr('data-id');

            Utils.goToFilm(filmId, this.collection);
        },


        onRemoveWatchlistFilmClick: function(e) {
            e.preventDefault();

            var $thisButton = $(e.currentTarget),
                modelId = $thisButton.closest('.js-film-search-result').find('.js-film-entry').attr('data-id');

            // find the model
            var film = this.collection.get(modelId);

            // Removes model from the collection
            this.collection.remove(film);

            Backbone.Events.trigger('directlyRemovedFromWatchlist', modelId);
        }

    });
});