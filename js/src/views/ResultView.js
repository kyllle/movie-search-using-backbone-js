define([
    'backbone',
    'templates',
    'src/views/DetailedFilmView',
    'src/models/Film'
], function(
    Backbone,
    templates,
    DetailedFilmView,
    Film
) {
    'use strict';

    /**
     *  Focuses on each individual film module
     *  At the moment we just select the template to be used,
     *  and pass the model through as JSON
     */
    return Backbone.View.extend({


        className: 'film-search-result js-film-search-result',


        template: templates.films,


        initialize: function() {
            this.listenTo(this.model, 'change:watchlist', this.render);
            this.listenTo(this.model, 'remove', this.remove);

            this.render();
        },


        render: function() {
            this.$el.html( this.template(this.model.toJSON()) );
        }

    });
});