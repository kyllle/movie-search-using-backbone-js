define([
    'backbone',
    'templates',
    'src/utils/utils'
], function(
    Backbone,
    template,
    utils
) {
    'use strict';

    return Backbone.View.extend({


        className: 'detailed-film-view',


        template: templates.film,


        events: {
            'click .js-watchlist': 'onToggleWatchlistClick'
        },


        initialize: function() {

            // listen to the sync of model
            this.listenTo(this.model, 'sync', this.render);
            this.listenTo(this.model, 'change:watchlist', this.render);

            // fetch more data
            this.model.fetch();
        },


        render: function() {

            // call the render to stick into DOM
            this.$el.html( this.template(this.model.toJSON()) );

            return this;
        },


        // Note: we are gonna call a sub function (helper function)
        onToggleWatchlistClick: function(e) {
            e.preventDefault();

            var $thisButton = $(e.currentTarget),
                buttonToggle = $thisButton.attr('data-toggle');

            // Pass our model into the utility function to be removed from the watchlist collection
            utils.toggleWatchlist(this.model, buttonToggle);
        }

    });
});