define([
    'backbone',
    'src/utils/config',
    'src/views/ResultView',
    'src/router',
    'src/utils/utils'
], function(
    Backbone,
    config,
    ResultView,
    router,
    utils
) {
    'use strict';

    /**
     *  Search Results Collection View
     */

    return Backbone.View.extend({


        className: 'search-results-view main-ctn',


        template: templates.films,

        // Note on event naming conventions
        // Better to name event methods with the event type, so js-watchlist-add = onWatchlistAddClick
        // This makes things a lot more readable and seperates these methods from other methods that are non event based
        events: {
            'click .js-film-entry': 'onFilmEntryClick',
            'click .js-watchlist': 'onToggleWatchlistClick'
        },


        selectors: {},


        initialize: function() {

            // listens to a change in the collection by the sync event and calls the render method
            this.listenTo(this.collection, 'sync', this.render);
        },


        render: function() {

            var self = this,
                gridFragment = this.createItems();

                this.$el.html(gridFragment);

            return this;
        },


        createItems: function() {

            var self = this,
                gridFragment = document.createDocumentFragment();

                this.collection.each(function (film) {
                    var filmView = new ResultView({
                        'model': film
                    });

                    gridFragment.appendChild(filmView.el);

                }, this);


            return gridFragment;

        },


        onFilmEntryClick: function(e) {
            e.preventDefault();

            // Get the clicked elements film id
            var filmId = $(e.currentTarget).attr('data-id');

            Utils.goToFilm(filmId, this.collection);
        },


        // Note: we are gonna call a sub function (helper function)
        onToggleWatchlistClick: function(e) {
            e.preventDefault();


            var $thisButton = $(e.currentTarget),
                modelId = $thisButton.closest('.js-film-search-result').find('.js-film-entry').attr('data-id'),
                buttonToggle = $thisButton.attr('data-toggle');

            // find the model
            var film = this.collection.get(modelId);


            utils.toggleWatchlist(film, buttonToggle);
        }

    });
});