define([
    'jquery',
    'underscore',
    'backbone',
    'requireDomReady',
    'handlebars',
    'templates',
    'src/utils/config',
    'src/utils/dataStore',
    'src/utils/utils'
], function(
    $,
    _,
    Backbone,
    requireDomReady,
    Handlebars,
    templates,
    config,
    dataStore,
    Utils
) {
    'use strict';

    require([
        'src/app'
    ], function (
        app
    ) {
        // IE console shim.
        if ( !window.console ) {
            window.console = {
                log: function() {},
                dir: function() {}
            };
        }

        requireDomReady(app);
    });
});