define([
    'backbone',
    'jquery',
    'src/router'
], function (
    Backbone,
    $,
    router
) {
    'use strict';


    // Setting up var Utils = {} is good when you need to make references like Utils.isActive = false
    // This helps with scoping instead of just returning the object and this meaning could get lost easily
    var Utils = {

        //pass model to here, get dataStore.watchlist
        toggleWatchlist: function(model, toggleType) {


            var toggle;
            switch (toggleType) {
                case 'add':
                    toggle = 'add';
                    break;
                case 'remove':
                    toggle = 'remove';
                    break;
            }


            // Now trigger and event
            Backbone.Events.trigger(toggle+':watchlist', model);
        },


        goToFilm: function(filmId, collection) {

            // Get the correct model from the collection based on the clicked elements id
            var film = collection.get(filmId);

            // Fire off event to say this model is ready to be fetched and shown
            Backbone.Events.trigger('search:film', film);

            router.navigate('film/'+film.get('id'), { trigger:false });
        }

    };

    return Utils;
});