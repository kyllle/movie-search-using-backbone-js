define([
    'jquery'
], function (
    $
) {
    'use strict';

    // Individual Movie Request http://api.themoviedb.org/3/movie/62177?api_key=6b5ff1a81fe75e221731a3f81daedcfe

    var config = {
        apiKey: '6b5ff1a81fe75e221731a3f81daedcfe'
    };

    return config;
});