define([
    'src/collections/Films',
    'src/collections/WatchlistFilms'
], function(
    Films,
    WatchListFilms
) {
    'use strict';


    /**
     *  Purpose of the dataStore
     *  This is where we set request the data to be used in each of our collections,
     *  this data could be requested via AJAX, static data printed out in the DOM etc.
     *
     *  In order to instatiate our collections and models we have to make sure
     *  these scripts are defined in by requirejs
     */


    // To be returned at the end of this module.
    var dataStore = {};

    // Example - notice dataStore gets used as this is the name of the file
    // dataStore.locales = new Locales(window.bootstrappedLocales);

    /**
     *  First of all instantiate the Films Backbone Collection.
     */
    dataStore.films = new Films();
    dataStore.watchListFilms = new WatchListFilms();

    /**
     *  Running the Collections fetch method to request data returns a success
     *  and error method similar to jQuery with the collection, response and options @params
     */
    // dataStore.films.fetch({
    //     success: function(collection, response, options) {
    //         console.log('dataStore.films dataStore = SUCCESS');
    //         console.log('Collection', collection);
    //         console.log('Response', response);
    //         console.log('Options', options);
    //         console.log('---');
    //     },
    //     error: function(collection, response, options) {
    //         console.log('dataStore.films dataStore = FAIL');
    //         console.log(collection);
    //         console.log(response);
    //         console.log(options);
    //         console.log('---');
    //     }
    // });



    return dataStore;
});