define([
    'handlebars'
], function(
    Handlebars
) {
    'use strict';

    /**
     *  Register a Handlebars helper to make requested values lowercase
     *  Example: {{#tolower}}{{title}}{{/tolower}}
     */
    Handlebars.registerHelper('tolower', function(options) {
        return options.fn(this).toLowerCase();
    });

});