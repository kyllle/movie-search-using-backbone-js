define([
    'backbone',
    'src/views/MainAppView',
    'src/router' //already contructed in router.js as a function that can be shared across the app, doesn't need to be an individual constructor for each
], function(
    Backbone,
    MainAppView,
    router
) {
    'use strict';

    return function() {

        // Create an app view which will hold the base markup in a template
        // render the app view and this will go and kick init the app creating each section

        // Instantiating and rendering homepage/start page setting #main as our main selector reference
        var mainAppView = new MainAppView({
            'el': '#main'
        });
        mainAppView.render();


        // Starts history api, will begin listening for history fragments
        Backbone.history.start();

    };
});