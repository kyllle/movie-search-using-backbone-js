require.config({
    deps: ['src/boot'],
    baseUrl: 'js/',
    paths: {
        'requireJS': 'libs/requirejs/require',
        'requireDomReady': 'libs/requirejs-domready/domReady',
        'jquery': 'libs/jquery/jquery',
        'backbone': 'libs/backbone/backbone',
        'underscore': 'libs/underscore/underscore',
        'handlebars': 'libs/handlebars/handlebars',
        'templates': 'src/templates'
    },
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'handlebars': {
            exports: 'Handlebars'
        }
    }
});