define([
    'backbone',
    'src/models/Film',
    'src/utils/config'
], function (
    Backbone,
    Film,
    config
) {
    'use strict';

    /**
     *  Films requested on search
     *  Associated model is Film
     *
     *
     */
    return Backbone.Collection.extend({


        model: Film,


        searchTerm: '',


        initialize: function() {

            Backbone.Events.on('directlyRemovedFromWatchlist', this.resetWatchlist, this);
        },


        url: function() {
            return 'http://api.themoviedb.org/3/search/movie?query='+encodeURIComponent(this.searchTerm)+'&api_key='+config.apiKey+''
        },


        getFilms: function(searchTerm) {

            console.log('Films.getFilms', searchTerm);

            // Update the search term property which will then be updated when the url method is run
            // Note make sure any url changes are made BEFORE calling fetch
            this.searchTerm = searchTerm;

            this.fetch();
        },


        resetWatchlist: function(modelId) {

            // Loop through collection to see if model exists
            var film = this.get(modelId);

            // If exists set watchlist: false
            if(film) {
                film.set('watchlist', false);
            }

        },


        /**
         *  When the data is fetched particularly in third party api you will get a lot of
         *  data back thats not required, the only thing you actually need is the results
         *  you eventually want to template out. By using the parse method you can drill down
         *  to what you need and return that.
         *
         *  response.results means my new data is just the results
         */
        parse: function(response) {
            return response.results;
        }

    });


});