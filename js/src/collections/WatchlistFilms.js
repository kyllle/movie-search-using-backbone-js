define([
    'backbone',
    'src/models/Film'
], function(
    Backbone,
    Film
) {
    'use strict';

    return Backbone.Collection.extend({


        model: Film


    });
});