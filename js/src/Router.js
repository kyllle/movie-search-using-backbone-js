define([
    'backbone'
], function(
    Backbone
) {
    'use strict';

    var Router = Backbone.Router.extend({


        routes: {
            '': 'showHome',
            'film/:id': 'showFilm',
            'search/:term': 'showHome',
            'watchlist/': 'showWatchlist'
        },


        showHome: function(term) {
            console.log('route show:home')
            Backbone.Events.trigger('show:home', term);
        },


        showFilm: function(id) {
            console.log('route search:film ' + id);

            // Trigger event to say film has been requested and pass film id
            Backbone.Events.trigger('search:film', id);
        },


        showWatchlist: function() {
            console.log('route show:watchlist');

            Backbone.Events.trigger('show:watchlist');
        }

    });

    return new Router();
});