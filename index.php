<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">

        <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/base.css">
    </head>
    <body>



        <div id="main"></div>











        <!-- if in production
            <script src="js/main.min.js"></script>
        -->

        <script src="js/libs/requirejs/require.js" data-main="js/src/config.r"></script>
    </body>
</html>