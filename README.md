Project Skeleton
=========

This skeleton outlines an example project architecture for front-end development including build process. Using grunt as a build tool, we are able to do the following:

  - Compile, concatenate and minify SASS.
  - Lint, concatenate and uglify JS.
  - Watch source files for changes and trigger compilation / minification.
  - Perform lossless optimization on image assets.

Folder Structure
-
Both stylesheets and scripts follow the same structure. Library files are placed in 'libs' and are normally not edited. These library files do not have to be minifed and in best practice probably shouldn't be. This is because during development, errors within them are easier to debug, and also that the build scripts will be minifying them anyway. All source files are placed within 'src' and are split into modular files to aid in decoupling and organisation. Build files that are the end result of compilation are placed within the root of the 'css' or 'js' folders. Images are placed within an 'img' folder and should be maintained by grouping related imagery (features, sections etc..) into sub-folders.

CSS Notes
-
CSS development uses SASS as a pre-processor and Compass as a framework. You will note that there are source files prefixed with underscores. This tells the SASS compiler that they should not be compiled into standalone CSS files, and are actually partials to be imported into the master stylesheet (which is the only unprefixed stylesheet there is).

Project-wide variables and mixins should be created and added to their respective partials. The order of the master stylesheet ensures these are both available before any styling occurs. General and reused styles should be kept in the master stylesheet, with all other modular styling happening in underscore prefixed partial stylesheets.

JS Notes
-
The JS architecture has been left plain deliberately. There are a multitude of patterns and methodologies to choose from, and some are likely more suitable for particular projects than others. All of these patterns will fit the simple 'lib' and 'src' folder structure, however you should build upon this and adapt the skeleton to suit your needs. If you need an MVC architecture, you can setup sub-folders in 'src' for 'views', 'models' and so on. If you wish to use RequireJS or another AMD library, you can do so and remove 'uglify' as a Grunt module and replace it with whatever is appropriate. Build files should be left in the root of the 'js' folder for consistency with the CSS workflow.

Build Procedure
-
Grunt can watch directories for changes to library and source files, then automatically trigger appropriate build procedures. You can do this using the watch feature as seen below:

```
grunt watch;
```

You can also trigger the build procedure manually for both CSS and JS compilation:

```
grunt compass;
grunt uglify;
```

If you wish to lint your JS source files and ensure they are standards-compliant then run the following:

```
grunt jshint;
```

Towards the end of your project, you may find it beneficial to perform loseless compression on image assets. To do so, run the following:

```
grunt imagemin;
```

Installation
-
Grunt runs on Node and installing this platform and its packages are already well documented at its official website. Useful links are as follows:

* [Node] - The platform.
* [Npm] - A package manager included in Node.
* [Grunt] - The module written upon Node.

[Grunt]: http://gruntjs.com/
[Node]: http://nodejs.org/
[Npm]: https://npmjs.org/

After node is installed, you will need to install ruby gems for SASS and Compass. Ensure you have ruby installed and then run the following:

```
gem install sass;
gem install compass;
```

Once the environment is setup, we must install the node dependencies for this skeleton. The node modules are not stored in the repo itself as they are a developer dependency, not a project dependency. To install them, run the following in your terminal from the root directory of the repository:

```
npm install;
```

After these dependencies are downloaded and installed, you can run grunt and begin using its build tools.# This is my README
